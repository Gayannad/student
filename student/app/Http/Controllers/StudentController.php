<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{

    public function studentSave(Request $request)
    {

        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $address = $request['address'];
        $student_no = $request['student_no'];

        $student = new Student();
        $student->first_name = $first_name;
        $student->last_name = $last_name;
        $student->address = $address;
        $student->student_number = $student_no;
        $student->save();
        return redirect()->to('/student/list');

    }

    public function studentList()
    {
        $students = Student::orderBy('id', 'desc')->paginate(10);
        return view('layouts.list',compact('students'));
    }

}
