@extends('layouts.app')
<meta name="csrf-token" content="{!! csrf_token() !!}">
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Student
                <a href="/student/list">
                    <button class="btn btn-success btn-sm float-right">Student List</button>
                </a>
            </div>
            <div class="card-body form-group">
                <form>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="">First Name</label>
                                <input type="text" class="form-control{{$errors->has('first_name') ? ' is-invalid' :''}}" name="first_name" id="first_name">
                            </div>
                            <div class="form-group">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control{{$errors->has('last_name') ? ' is-invalid' :''}}" name="last_name" id="last_name">
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input type="text" class="form-control{{$errors->has('address') ? ' is-invalid' :''}}" id="address" name="address">
                            </div>
                            <div class="form-group">
                                <label for="">Student No</label>
                                <input type="text" name="student_no" id="student_no" class="form-control{{$errors->has('student_no') ? ' is-invalid' :''}}">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                            </div>
                            <div class="row float-right">
                                <button type="button" class="btn btn-primary btn-sm" onclick="saveStudent()">Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>

        function saveStudent() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var address = $('#address').val();
            var student_no = $('#student_no').val();
            $.ajax({
                url: '/student/save',
                method: 'post',
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    address: address,
                    student_no: student_no
                }

            }).done(function (response) {
                $('#first_name').val('');
                $('#last_name').val('');
                $('#address').val('');
                $('#student_no').val('');
            });
        }



    </script>

@endsection
