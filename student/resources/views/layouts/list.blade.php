<?php
/**
 * Created by IntelliJ IDEA.
 * User: gayan
 * Date: 11/14/18
 * Time: 9:07 AM
 */ ?>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Student List
                <a href="/home">
                    <button class="btn btn-success btn-sm float-right">New Student</button>
                </a>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Fist Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Student No</th>
                    </tr>
                    @foreach($students as $student)
                        <tr>
                            <td>{{$student->id}}</td>
                            <td>{{$student->first_name}}</td>
                            <td>{{$student->last_name}}</td>
                            <td>{{$student->address}}</td>
                            <td>{{$student->student_number}}</td>
                        </tr>
                    @endforeach
                </table>
                {!! $students->render() !!}
            </div>
        </div>
    </div>
@endsection
